#
# Cookbook Name:: app_rights
# Recipe:: deploy
#
# Copyright 2013, Scholastic
#

node.default['php']['directives']['post_max_size'] = node['app_rights']['php']['directives']['post_max_size']
node.default['php']['directives']['upload_max_filesize'] = node['app_rights']['php']['directives']['upload_max_filesize']

include_recipe "schl_drupal_deploy"

