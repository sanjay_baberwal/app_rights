#
# Cookbook Name:: app_rights
# Recipe:: publish
#
# Copyright 2013, Scholastic
#

include_recipe "app_rights::deploy"

schl_drupal_deploy "rights" do
  action :deploy
  is_admin false
  data_bag "app_rights"
  deploy_vhost false
end

include_recipe "app_rights::post_deploy"

