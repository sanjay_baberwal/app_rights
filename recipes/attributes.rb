#
# Cookbook Name:: app_rights
# Recipe:: attributes
#
validate_attributes do
	attributes [["app_rights","deploy","git","repo"],
		["app_rights","domain_name"],
		["services","db","rights","host"],
		["services","db","rights","name"]]
end
