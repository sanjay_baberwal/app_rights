#
# Cookbook Name:: app_rights
# Recipe:: admin
#
# Copyright 2013, Scholastic
#

include_recipe "app_rights::deploy"

schl_drupal_deploy "rights" do
  action :deploy
  is_admin true
  data_bag "app_rights"
end

include_recipe "app_rights::post_deploy"

#execute "build.sh" do
#  command "./buildscripts/build.sh"
#  cwd "#{node['schl_drupal_deploy']['path']}/rights"
#  user node['schl_deploy']['user']
#end

sec = Chef::EncryptedDataBagItem.load_secret('/etc/chef/encrypted_data_bag_secret')
library = Chef::EncryptedDataBagItem.load("app_rights", node.chef_environment, sec)


execute "drush upwd" do
  command "drush upwd --password='#{library['app_settings']['admin_user']}' #{library['app_settings']['admin_pass']}"
  cwd "#{library['app_settings']['app_path']}"
  user library['system']['username']
end

