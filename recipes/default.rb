#
# Cookbook Name:: app_rights
# Recipe:: default
#
# Copyright 2013, Scholastic
#

log "empty recipe warning" do
  level :warn
  message "Default recipe app_rights was used. Please use a Drupal webserver role-specific recipe instead."
end

