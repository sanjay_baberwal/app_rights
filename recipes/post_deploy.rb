#
# Cookbook Name:: app_rights
# Recipe:: post_deploy
#
# Copyright 2013, Scholastic
#
#
sec = Chef::EncryptedDataBagItem.load_secret('/etc/chef/encrypted_data_bag_secret')
library = Chef::EncryptedDataBagItem.load("app_rights", node.chef_environment,sec)
app_web_root = library['app_settings']['app_path']
app_name = library['app_settings']['app_name']

include_recipe "app_rights::mailer"

# Delete directory before creating symlink
directory "#{app_web_root}/content" do
  action :delete
  only_if { File.directory?("#{app_web_root}/content") == true && !File.symlink?("#{app_web_root}/content") }
end

directory "/vol/#{app_name}/content" do
  owner 'apache'
  group 'apache'
  mode 00777
  action :create
end

#I don't believe we need this
link "#{app_web_root}/content" do
  owner node['schl_deploy']['user']
  to "/vol/#{app_name}/content"
end



execute "remove-stale-settings.php" do
  command "rm #{app_web_root}/sites/default/settings.php"
  only_if { File.exist?("#{app_web_root}/sites/default/settings.php") == true }
end

# content IO.read() won't work here as chef fails immediately if the file doesn't exist even if it will exist before this block is executed.
execute "copy-settings.php" do
  command "cp #{app_web_root}/profiles/rights_profile/settings.php #{app_web_root}/sites/default/settings.php"
  only_if { !File.exists?("#{app_web_root}/sites/default/settings.php") }
  notifies :create, "file[settings.php]", :immediately
end

file "settings.php" do
  path "#{app_web_root}/sites/default/settings.php"
  owner node['schl_deploy']['user']
  mode 00644
  action :nothing
  notifies :restart, "service[apache2]"
end

#cookbook_file "#{app_web_root}/sites/default/settings.php" do                                                                                                                                                                                                                  
#  source "settings.php"
#end

file "local.settings.php" do
  path "#{app_web_root}/sites/default/local.settings.php"
  owner node['schl_deploy']['user']
  mode 00644
  action :delete
end


#TO_DO: add a front end attribute which defines the app_name
#the app_name variable utilized in previous is steps is no longer available..
#this is a product of how the LWRPs are constructed....


sec = Chef::EncryptedDataBagItem.load_secret('/etc/chef/encrypted_data_bag_secret')
#db = Chef::EncryptedDataBagItem.load("app_rights", node.chef_environment,sec)
puts "-----------stacker:resource_name--" + node['stacker']['resource_name']
db = Stacker::ConfigData.get(node['stacker']['resource_name']);

template "#{app_web_root}/sites/default/local.settings.php" do
  source "local.settings.php.erb"
  owner node['schl_deploy']['user']
  mode '644'
  variables({
    :db_user => db['database']["user"],
    :db_pass => db['database']["pass"],
    :db_host => db['database']["host"],
    :db_name => db['database']["name"],
    :siteurl => node['application']['role'] == "admin" ? library['app_settings']['global_admin_domain'] : library['app_settings']['global_domain']
  })
end



file "/etc/httpd/sites-available/#{app_name}.conf" do
  action :delete
end

template "/etc/httpd/sites-available/#{app_name}.conf" do
  source "rce.conf.erb" 
  cookbook "app_rights"
  owner node['schl_deploy']['user']
  mode '644'
  variables({
    :server_name => node['application']['role'] == "admin" ? library['app_settings']['global_admin_domain'] : library['app_settings']['global_domain'],
    :server_aliases => library['app_settings']['global_domain_alias'],
    :newrelic_name => library['mgmt']['newrelic']['name'],
    :web_root => app_web_root,
    :appname => library['app_settings']['app_name']
  })
end


link "/etc/httpd/sites-enabled/#{app_name}.conf" do
  to "/etc/httpd/sites-available/#{app_name}.conf"
end

## Deploy app.settings.php
template "#{app_web_root}/sites/default/app.settings.php" do
  source "app.settings.php.erb"
  owner node['schl_deploy']['user']
  mode '644'
  variables({
    :varnish => library['varnish'],
    :app_settings => library['app_settings']
  })
end

#a.run_list.to_a.flatten.include?("app_#{app_name}_admin")



directory "#{app_web_root}/buildscripts" do
  owner node['schl_deploy']['user']
  action :create
end

link "#{app_web_root}/buildscripts/build.sh" do
  owner node['schl_deploy']['user']
  to "#{app_web_root}/profiles/rights_profile/scripts/build.sh"
end

file "#{app_web_root}/.htaccess" do
  action :delete
  owner node['schl_deploy']['user']
  only_if { File.exists?("#{app_web_root}/.htaccess") } 
end

cookbook_file "#{app_web_root}/.htaccess" do
  source "htaccess"
  owner node['schl_deploy']['user']
end

cookbook_file "#{app_web_root}/sites/default/files/.htaccess" do 
  source "files-htaccess"
  owner node['schl_deploy']['user']
end

execute "permission-site-root" do
  command "chown -R apache:apache /var/www/drupal/rights/sites/default/files"
end

execute "permission-images-root" do
  command "chmod 755 /var/www/drupal/rights/sites/default/files/images"
end

execute "permission-files-root" do
  command "chown -R apache:apache /vol/rights"
end
#enable mailer
include_recipe "app_rights::mailer"
