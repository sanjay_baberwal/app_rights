#
# Cookbook Name:: app_rights
# Recipe:: mailer
#
# Copyright 2014, Scholastic
#

sec = Chef::EncryptedDataBagItem.load_secret('/etc/chef/encrypted_data_bag_secret')
library = Chef::EncryptedDataBagItem.load("app_rights", node.chef_environment,sec)

#define resource for *reloading* postfix configuration
execute "reload-postfix" do
  action :nothing
  command "postfix reload"
end

#write out main.cf
template "/etc/postfix/main.cf" do
  source "main.cf.erb"
  variables({
    :hostname => node[:fqdn],
    :domain => library['messaging']['smtp']['domain'],
    :relay => library['messaging']['smtp']['relay']
  })
  notifies :run, "execute[reload-postfix]", :immediately
end
