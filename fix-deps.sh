#!/usr/bin/env sh

find ./metadata.rb -type f -name metadata.rb -exec sed -i  's#schl_aws_drupal_deploy#schl_drupal_deploy#g' '{}' \;
find ./README.md -type f -name README.md -exec sed -i 's#schl_aws_drupal_deploy#schl_drupal_deploy#g' '{}' \;
find ./attributes -type f -name \*.rb -exec sed -i 's#schl_aws_drupal_deploy#schl_drupal_deploy#g' '{}' \;
find ./recipes -type f -name \*.rb -exec sed -i 's#schl_aws_drupal_deploy#schl_drupal_deploy#g' '{}' \;
