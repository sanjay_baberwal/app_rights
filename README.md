Description
===========

Deploys Heads Up Drupal application.

Requires
========

This cookbook requires the following cookbooks to be present:

* `schl_drupal_base` >= 0.0.0
* `schl_drupal_deploy` >= 0.0.0
* `attributes_validator` >= 0.0.0
* `schl_deploy` >= 0.0.0


Attributes
==========

###General attributes

* Mostly all application specific configuration variables/values have been moved to data bags within Chef.
* `['app_settings']['app_name']` - (required) This string value should be the OFFICIAL name of the application with no prefix/suffix.
* `['app_settings']['app_name']` - (required) This string value should be the name of the data bag to utilize for this application. 

###How To

* create data bag using the following naming scheme: app_name (example: app_rights)
* Use the following data bag format as a starter for DRUPAL applications.
* Update global_appname, global_envname
* Update gluster_enabled
* Update all subkeys accordingly
  * storage
  	* storage host, storage mount, storage type
  * varnish
  	* DEPRECATED (update the app_drupal_varnish_aws data bag)
  * database
     * db_host, db_user, db_pass, db_name
  * app_settings
     * FQDN, Aliases, admin user/pass, git repo, s3/global components settings, memcache enabling etc

`{
  "id": "es-dev4",
  "global_appname": "APP",
  "global_envname": "dev",
  "gluster_enabled": false,
  "backend_fqdn": "FQDN",
  "backend_aliases": "ALIAS1 ALIAS2 ALIAS3 ETC",
  "git_repo": "GIT_REPO",
  "git_ref": "master",
  "system": {
    "username": "svc-chefdeploy"
  },
  "storage": {
    "type": "glusterfs",
    "host": "ss-e1b-lfsd-003.scholastic.net",
    "mount": "research-dev"
  },
  "varnish": {
    "setup": {
      "enabled_backends": [
        "global_resources"
      ],
      "ent_varnish_backends": [
        "global_resources"
      ],
      "deploy_config_files": [
        "global_defaults.vcl",
        "purge.vcl"
      ],
      "storage_size": "6.5G",
      "disabled_caching": [

      ]
    },
    "hosts": [
      "127.0.0.1"
    ]
  },
  "database": {
    "host": "DB_HOST",
    "name": "DB_NAME",
    "user": "DB_USER",
    "pass": "DB_PASS"
  },
  "app_settings": {
    "build": {
      "ctrl_lwrp": {
        "skip_php_settings": false
      }
    },
    "app_name": "example",
    "app_path": "/var/www/drupal/example",
    "app_repo": "GIT_REPO",
    "admin_user": "admin",
    "admin_pass": "admin",
    "app_revision": "master",
    "enable_memcached": false,
    "memcached_bucket": "global",
    "global_domain": "researchdev1.scholastic.com",
    "global_domain_alias": [
      "researchdev1.scholastic.com"
    ],
    "global_asset_library": "",
    "global_include_type": "esi",
    "global_components_path": "global",
    "global_components_host": "globaldev1.scholastic.com",
    "global_components_domain": "globaldev1.scholastic.com",
    "global_resources_path": "global",
    "global_resources_host": "globaldev1.scholastic.com",
    "global_resources_domain": "globaldev1.scholastic.com",
    "enable_cache": "0",
    "enable_compression": "0",
    "cache_expire": "0",
    "s3_host": "s3.amazon.com",
    "s3_bucket": "test"
  }
}`
