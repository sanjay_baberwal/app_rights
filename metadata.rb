maintainer       "Scholastic"
maintainer_email "asamuels-consultant@scholastic.com"
license          "Apache 2.0"
description      "Deploys Scholasic Rights & Coeditions Application. (Drupal)"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "1.0.7"
depends          "schl_drupal_base"
depends          "schl_drupal_deploy", ">= 2.0.0" #utilize the AWS incarnation of the deploy LWRP
depends          "attributes_validator"
depends          "schl_deploy"


