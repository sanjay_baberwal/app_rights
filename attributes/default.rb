default['app_rights']['features']['memcached']['enabled'] = false
default['app_rights']['features']['memcached']['bucket_name'] = "rights"
default['app_rights']['features']['newrelic'] = nil
default['app_rights']['features']['glusterfs'] = nil

default['app_rights']['deploy']['git']['repo'] = "https://aaronpsamuel:1Y0ungluck1234@bitbucket.org/aaronpsamuel/research-and-coeditions-demo.git"
default['app_rights']['deploy']['compress_assets'] = true
default['app_rights']['deploy']['newrelic_appname'] = nil
default['app_rights']['deploy']['global']['asset_library'] = "teacher"
default['app_rights']['deploy']['global']['cache'] = "1"
default['app_rights']['deploy']['global']['cache_expire'] = "240"

default['app_rights']['php']['directives']['post_max_size'] = '20M'
default['app_rights']['php']['directives']['upload_max_filesize'] = '20M'

